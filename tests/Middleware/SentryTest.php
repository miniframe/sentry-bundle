<?php

namespace Miniframe\Middleware;

use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\InternalServerErrorResponse;
use Miniframe\Response\NotFoundResponse;
use PHPUnit\Framework\TestCase;
use Sentry\Event;
use Sentry\EventHint;
use Sentry\SentrySdk;

class SentryTest extends TestCase
{
    /**
     * Boolean set in anonymous function that hooks into Sentry
     *
     * @var bool
     */
    private $beforeSendTriggered = false;

    /**
     * Returns a list of test data for the logExceptionsToSentry tes
     *
     * @return array[]
     */
    public function logExceptionsToSentryDataProvider(): array
    {
        return array(
            'Standard exception' => [
                'response'    => new InternalServerErrorResponse('', 0, new \RuntimeException()),
                'throwable'   => true,
                'logToSentry' => true,
            ],
            '404 exception, is in exclusion list' => [
                'response'    => new NotFoundResponse(),
                'throwable'   => true,
                'logToSentry' => false,
            ],
            'Regular response, not thrown, no exit code' => [
                'response'    => new Response(),
                'throwable'   => false,
                'logToSentry' => false,
            ],
            'Regular response, but thrown' => [
                'response'    => new Response(),
                'throwable'   => true,
                'logToSentry' => true,
            ],
            'Regular response, but with exit code' => [
                'response'    => new Response('', 1),
                'throwable'   => false,
                'logToSentry' => true,
            ],
        );
    }

    /**
     * Test tag related methods
     *
     * @return void
     */
    public function testTags(): void
    {
        $middleware = new Sentry(Request::getActual(), $this->getDummyConfig());

        // Set and get tag
        $middleware->setTag('foo', 'bar');
        $this->assertEquals('bar', $middleware->getTag('foo'));

        // Unset tag (and validate)
        $middleware->unsetTag('foo');
        $this->assertNull($middleware->getTag('foo'));

        // Set multiple tags
        $middleware->setTags(['foo' => 'bar', 'baz' => '1337']);
        $this->assertEquals('bar', $middleware->getTag('foo'));
        $this->assertEquals('1337', $middleware->getTag('baz'));

        // Unset tags (and validate)
        $middleware->unsetTags('foo', 'baz');
        $this->assertNull($middleware->getTag('foo'));
        $this->assertNull($middleware->getTag('baz'));
    }

    /**
     * Test extras related methods
     *
     * @return void
     */
    public function testExtras(): void
    {
        $middleware = new Sentry(Request::getActual(), $this->getDummyConfig());

        // Set and get tag
        $middleware->setExtra('foo', 'bar');
        $this->assertEquals('bar', $middleware->getExtra('foo'));

        // Unset tag (and validate)
        $middleware->unsetExtra('foo');
        $this->assertNull($middleware->getExtra('foo'));

        // Set multiple tags
        $middleware->setExtras(['foo' => 'bar', 'baz' => '1337']);
        $this->assertEquals('bar', $middleware->getExtra('foo'));
        $this->assertEquals('1337', $middleware->getExtra('baz'));

        // Unset tags (and validate)
        $middleware->unsetExtras('foo', 'baz');
        $this->assertNull($middleware->getExtra('foo'));
        $this->assertNull($middleware->getExtra('baz'));
    }

    /**
     * Test if getPostProcessors return an array with callables
     *
     * @return void
     */
    public function testGetPostProcessors(): void
    {
        $middleware = new Sentry(Request::getActual(), $this->getDummyConfig());
        $routers = $middleware->getPostProcessors();
        $this->assertGreaterThan(0, count($routers));
        foreach ($routers as $router) {
            $this->assertIsCallable($router);
        }
    }

    /**
     * Tests the logExceptionsToSentry method
     *
     * @param Response $response       Response object.
     * @param boolean  $thrown         True when thrown.
     * @param boolean  $loggedToSentry True when the exception should be logged.
     *
     * @return void
     *
     * @dataProvider logExceptionsToSentryDataProvider
     */
    public function testLogExceptionsToSentry(Response $response, bool $thrown, bool $loggedToSentry): void
    {
        // Initialize middleware
        $middleware = new Sentry(Request::getActual(), $this->getDummyConfig());

        // Hook into Sentry, so we can capture exceptions
        \Sentry\init(['dsn' => '', 'before_send' => function (Event $event) {
            $this->beforeSendTriggered = true;
        }]);

        // Log exception
        $this->beforeSendTriggered = false;
        $result = $middleware->logExceptionsToSentry($response, $thrown);

        // The result should be the same response, unmodified
        $this->assertEquals($response, $result);
        // Is 'before_send' triggered?
        $this->assertEquals($loggedToSentry, $this->beforeSendTriggered);
    }

    /**
     * Tests the before_send option
     *
     * @return void
     */
    public function testBeforeSendTagsAndExtras(): void
    {
        // Initialize middleware
        $middleware = new Sentry(Request::getActual(), $this->getDummyConfig());

        // Create a dummy event
        $event = Event::createEvent();
        $eventHint = EventHint::fromArray(['exception' => new \RuntimeException('Foobar')]);

        // Add tag and extra
        $middleware->setExtra('foo', 'bar');
        $middleware->setTag('foo', 'bar');

        // Execute the beforeSend event
        $beforeSend = SentrySdk::getCurrentHub()->getClient()->getOptions()->getBeforeSendCallback();
        $newEvent = $beforeSend($event, $eventHint); /* @var $newEvent Event */

        $this->assertEquals(['foo' => 'bar'], $newEvent->getExtra());
        $this->assertEquals(['foo' => 'bar'], $newEvent->getTags());
    }
    /**
     * Tests the before_send option
     *
     * @return void
     */
    public function testBeforeSendIgnoreExceptionType(): void
    {
        // Initialize middleware
        $middleware = new Sentry(Request::getActual(), $this->getDummyConfig());

        // Create a dummy event
        $event = Event::createEvent();
        $eventHint = EventHint::fromArray(['exception' => new NotFoundResponse()]);

        // Execute the beforeSend event
        $beforeSend = SentrySdk::getCurrentHub()->getClient()->getOptions()->getBeforeSendCallback();
        $newEvent = $beforeSend($event, $eventHint); /* @var $newEvent Event */

        $this->assertNull($newEvent);
    }

    /**
     * Returns a fictive configuration based on an array (can be used for several tests)
     *
     * @return Config
     */
    private function getDummyConfig(): Config
    {
        return Config::__set_state([
            'configFolder' => __DIR__,
            'projectFolder' => __DIR__,
            'data' => ['sentry' => [
                'dsn' => '',
                'exclude' => [
                    NotFoundResponse::class,
                ],
            ]]
        ]);
    }
}
