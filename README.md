# Miniframe Sentry Bundle

This library adds Sentry support to the [Miniframe PHP Framework](https://miniframe.dev/).

[![build](https://miniframe.dev/build/badge/sentry-bundle)](https://bitbucket.org/miniframe/sentry-bundle/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/sentry-bundle)](https://miniframe.dev/codecoverage/sentry-bundle)

## How to use

1. In your existing project, type: `composer require miniframe/sentry-bundle`
2. Add the directive below to your config.ini

*Example config.ini directives*
```ini
[framework]
middleware[sentry] = Miniframe\Middleware\Sentry

[sentry]
dsn = https://******************@******.ingest.sentry.io/******
exclude[] = Miniframe\Response\NotFoundResponse
```

Add the Middleware as high as possible in the Middleware list,
so it's loaded before other middlewares get the chance of throwing errors.

You can add a list of error classes that shouldn't be logged, to save on API calls to Sentry.

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/sentry-bundle/addon/pipelines/home) to automatically test this project.
