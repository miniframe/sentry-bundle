<?php

namespace Miniframe\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\InternalServerErrorResponse;

use function Sentry\init as InitSentry;
use function Sentry\captureException;

class Sentry extends AbstractMiddleware
{
    /**
     * List of fully qualified classnames that shouldn't be logged when sent to Sentry
     *
     * @var string[]
     */
    private $exclusions = array();

    /**
     * List of tags to be sent to Sentry
     *
     * @var array
     */
    private $tags = array();

    /**
     * List of extra data to be sent to Sentry
     *
     * @var array
     */
    private $extra = array();

    /**
     * Initializes the Sentry middleware
     *
     * @param Request $request The request object.
     * @param Config  $config  The config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        InitSentry([
            'dsn' => $config->get('sentry', 'dsn'),
            'before_send' => function (\Sentry\Event $event, ?\Sentry\EventHint $hint): ?\Sentry\Event {
               // Ignore excluded exceptions
                if (
                    $hint !== null && (
                        in_array(get_class($hint->exception), $this->exclusions)
                        || (
                            $hint->exception->getPrevious()
                            && in_array(get_class($hint->exception->getPrevious()), $this->exclusions)
                        )
                    )
                ) {
                    return null;
                }

                $event->setTags($this->tags);
                $event->setExtra($this->extra);

                return $event;
            },
        ]);

        $this->exclusions = $config->has('sentry', 'exclude') ? $config->get('sentry', 'exclude') : [];
    }

    /**
     * Returns a list of the post processors
     *
     * @return callable[]
     */
    public function getPostProcessors(): array
    {
        return [[$this, 'logExceptionsToSentry']];
    }

    /**
     * Defines a tag to be sent to Sentry. It's possible to filter by tag in Sentry.
     *
     * @param string      $label Tag label.
     * @param string|null $value Tag value.
     *
     * @return void
     */
    public function setTag(string $label, ?string $value): void
    {
        $this->tags[$label] = $value;
    }

    /**
     * Defines an extra value to be sent to Sentry. It's NOT possible to filter by this value in Sentry.
     *
     * @param string $label Extra label.
     * @param mixed  $value Extra data.
     *
     * @return void
     */
    public function setExtra(string $label, $value): void
    {
        $this->extra[$label] = $value;
    }

    /**
     * Defines a set of extra values to be sent to Sentry. It's NOT possible to filter by these values in Sentry.
     *
     * @param array $associativeArray Extra data (key => value pairs).
     *
     * @return void
     */
    public function setExtras(array $associativeArray): void
    {
        foreach ($associativeArray as $label => $value) {
            $this->setExtra($label, $value);
        }
    }

    /**
     * Defines a set of tags to be sent to Sentry. It's possible to filter by tag in Sentry.
     *
     * @param array $associativeArray Tags (key => value pairs).
     *
     * @return void
     */
    public function setTags(array $associativeArray): void
    {
        foreach ($associativeArray as $label => $value) {
            $this->setTag($label, $value);
        }
    }

    /**
     * Removes a tag to be sent to Sentry
     *
     * @param string $label Tag label.
     *
     * @return void
     */
    public function unsetTag(string $label): void
    {
        unset($this->tags[$label]);
    }

    /**
     * Removes a set of extra data to be sent to Sentry
     *
     * @param string $label Extra label.
     *
     * @return void
     */
    public function unsetExtra(string $label): void
    {
        unset($this->extra[$label]);
    }

    /**
     * Removes a set of tags to be sent to Sentry
     *
     * @param string ...$labels Tag labels.
     *
     * @return void
     */
    public function unsetTags(string ...$labels): void
    {
        foreach ($labels as $label) {
            $this->unsetTag($label);
        }
    }

    /**
     * Removes a set of extra data to be sent to Sentry
     *
     * @param string ...$labels Extra labels.
     *
     * @return void
     */
    public function unsetExtras(string ...$labels): void
    {
        foreach ($labels as $label) {
            $this->unsetExtra($label);
        }
    }

    /**
     * Gets a tag to be sent to Sentry
     *
     * @param string $label Tag label.
     *
     * @return string|null
     */
    public function getTag(string $label): ?string
    {
        return $this->tags[$label] ?? null;
    }

    /**
     * Gets an extra to be sent to Sentry
     *
     * @param string $label Extra label.
     *
     * @return mixed|null
     */
    public function getExtra(string $label)
    {
        return $this->extra[$label] ?? null;
    }

    /**
     * Sentry Post processor
     *
     * @param Response $response The current response.
     * @param boolean  $thrown   True when the response is thrown, otherwise false.
     *
     * @return Response
     */
    public function logExceptionsToSentry(Response $response, bool $thrown): Response
    {
        // Log all thrown responses, and responses with an exit code different from 0
        if (!$thrown && $response->getExitCode() === 0) {
            return $response;
        }

        if (is_a($response, InternalServerErrorResponse::class) && $response->getPrevious()) {
            if (!in_array(get_class($response->getPrevious()), $this->exclusions)) {
                captureException($response->getPrevious());
            }
        } else {
            if (!in_array(get_class($response), $this->exclusions)) {
                captureException($response);
            }
        }

        return $response;
    }
}
